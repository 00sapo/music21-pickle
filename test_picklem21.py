""" a module to test pickle module """
from music21 import corpus
s = corpus.parse('beethoven/opus18no1/movement1.mxl')
print('Loaded movement 1 from op 18, n 1 by Beethoven!')

from timeit import default_timer as timer
start = timer()
import picklem21 as pickle
end = timer()
print('* Importing picklem21 needed', end - start, 'seconds')

# test pickle and depickle to/from file


def test_file_rw():
    for METHOD in ['lzma', 'gzip', 'bz2', 'None']:
        pickle.setCompressionMethod(METHOD)
        start = timer()
        with open('pickle-test.pkl.' + METHOD, 'wb') as f:
            pickle.dump(s, f)
        end = timer()
        print('* Writing file using ' + METHOD +
              ' compression needed', end - start, 'seconds')

        start = timer()
        with open('pickle-test.pkl.' + METHOD, 'rb') as f:
            pickle.load(f)
        end = timer()
        print('* Reading file using ' + METHOD +
              ' compression needed', end - start, 'seconds')

# test multiprocessing


def test_multiprocessing():
    import multiprocessing as mp

    def parse_stream(s, start, end, tot_elements):
        """ parse a stream and count objects """
        count = 0
        for i in s.recurse()[start:end]:
            count += 1

        tot_elements.value += count

    total_length = len(s.recurse())
    nproc = 4
    for METHOD in ['lzma', 'gzip', 'bz2', 'None']:
        pickle.setCompressionMethod(METHOD)

        tot_elements = mp.Value('i', 0)
        start = 0
        processes = []
        for i in range(nproc):
            end = int(total_length / nproc * (i + 1))
            p = mp.Process(target=parse_stream, args=(
                s, start, end, tot_elements))
            processes.append(p)
            start = end

        for p in processes:
            p.start()

        for p in processes:
            p.join()

        print('# multiprocessing with ' + METHOD +
              ' counted ', tot_elements.value, 'elements')

    print('# actually the object has', len(s.recurse()), 'elements')


test_file_rw()
test_multiprocessing()
