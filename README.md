# music21-pickle
### A python module to build parallel music computing application and to make easier storing and restoring `music21` objects.

This repository contains a python module to be used with `music21` library. It has been tested with python 3.6.5. Simply use it with `import picklem21 as pickle`
and use it.

Available functions are:
* `pickle.dump()`
* `pickle.dumps()`
* `pickle.load()`
* `pickle.loads()`

Use `pickle.setCompressionMethod(METHOD)` to use a particular compression method, available values are 'lzma', 'gzip', 'bz2', 'None'.

Once loaded, you can import `multiprocessing` module and use `music21` in parallel computing applications.

You can find more examples in `test_picklem21.py` file.

## LICENSE
Copyright (c) 2018 Federico Simonetta, [federicosimonetta.it](http://federicosimonetta.it)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
