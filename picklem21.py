""" This module is a bridge to the standard pickle module. It allows to
serialize and deserialize objects, including for multiprocessing purposes or
savig to file.

You can simply use this module by setting a certain compression level - if
desired - and by calling dumb/dumps and load/loads objects. Not all
functionality from standard pickle module are provided. Note that only lzma,
gzip and bz2 compressions are supported
"""

import copyreg
import io
import pickle
import music21.converter
import music21.stream
import multiprocessing.reduction
# from lzma import decompress, compress

# COMPRESSION_LEVEL = -1
COMPRESSION_METHOD = 'None'


def compress(arg):
    """ Method to compress objects according to COMPRESSION_METHOD """
    return arg


def decompress(arg):
    """ Method to decompress objects according to COMPRESSION_METHOD """
    return arg


# simply proxy to standard pickle module
HIGHEST_PROTOCOL = pickle.HIGHEST_PROTOCOL
DEFAULT_PROTOCOL = pickle.DEFAULT_PROTOCOL


def setCompressionMethod(method: str):
    """ This method sets COMPRESSION_METHOD that will be used in pickling
    objects """
    COMPRESSION_METHOD = method

    if COMPRESSION_METHOD == 'lzma':
        from lzma import decompress, compress
        from lzma import LZMAFile as CompressedFile
    elif COMPRESSION_METHOD == 'gzip':
        from gzip import decompress, compress
        from gzip import GzipFile

        # the following is needed due to a not coherent syntax in standard
        # python libs
        global CompressedFile

        def CompressedFile(f):
            return GzipFile(fileobj=f)

    elif COMPRESSION_METHOD == 'bz2':
        from bz2 import decompress, compress
        from bz2 import BZ2File as CompressedFile
    elif COMPRESSION_METHOD == 'None':
        # defining dummy functions

        global CompressedFile

        def CompressedFile(arg):
            return arg

        global compress

        def compress(arg):
            return arg

        global decompress

        def decompress(arg):
            return arg
    else:
        raise Exception('compression method not supported')


def pickle_music21_stream(stream_obj):
    """ Function that can be added to a dispatch table. This returns two values:
        * a function to be used to deserialize
        * a tuple containing one value consisting of the serialization string
    """

    return music21.converter.thawStr, (music21.converter.freezeStr(stream_obj),)


def dumps(obj, protocol=DEFAULT_PROTOCOL):
    """ Pickle an object to byte values
    protocol: the one that you would use with standard pickle module """
    f = io.BytesIO()
    p = pickle.Pickler(f, protocol)
    p.dispatch_table = copyreg.dispatch_table.copy()
    p.dispatch_table[music21.stream.Stream] = pickle_music21_stream
    p.dump(obj)
    return compress(f.getvalue())


def dump(obj, file, protocol=DEFAULT_PROTOCOL):
    """ Pickle an object to file object already opened in byte mode.
    protocol: the one that you would use with standard pickle module """
    f = dumps(obj, protocol)
    file.write(f)


def loads(data):
    """ Depickle a byte object """
    # hook is registered in the pickle data
    data = decompress(data)
    return pickle.loads(data)


def load(file):
    """ Depickle a file object, already opened in byte mode"""
    file = CompressedFile(file)
    return pickle.load(file)


multiprocessing.reduction.register(music21.stream.Stream, pickle_music21_stream)
